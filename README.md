# Genbench technical requirements

This document describes the technical requirements for the proper functionning of the GenBench software.

## Hardware
- CPU: 8 Cores
- RAM: 16 GB
- Disk: 40 GB
- Additional storage might be needed to manage document files outside the database.
- It is strongly recommended to use 2 virtual machines, one for staging and one for production.

## Operating system
- Linux, including a GUI

## Software and  configuration
- NPM 
- Apache: Apache Frontend Server must be configured to use certificate.
- MySQL Server
- OpenJDK version 8

## Personal computers
- Microsoft Edge is recommended, with the needed extension for stickers printing.
- Personal computers must be configured to open csv files with the appropriate third-party software (e.g. Microsoft Excel or Word).

## Libraries and dependencies
- CKEditor: the CKEditor 5 software must be installed, including the following plugins:
    - CKEditor 5 Track Changes plugin
    - CKEditor 5 Pagination plugin
    - CKEditor 5 Comments plugin

## Network
- A working **secure** connection with reasonnable bandwith between the server and the staff personal computers must be guaranteed at all time.

